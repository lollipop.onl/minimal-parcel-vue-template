# Parcel x Vue minimal template

## Architectures

- [Parcel](https://parceljs.org/)
- [Vue](https://vuejs.org/)
- [Sass](https://sass-lang.com/)
- [Pug](https://pugjs.org/)

## Usage

```sh
$ git clone https://gitlab.com/lollipop.onl/minimal-parcel-vue-template.git <YOUR_PROJECT_NAME>

$ cd <YOUR_PROJECT_NAME>

$ npm install

$ npm start

# Start dev server on http://localhost:1234
```

## License

MIT
